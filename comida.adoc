== ﻿Lista de comida

// Ordenada por orden alfabético en cada apartado

=== Aperitivos

* Aceitunas
* Coctel de frutos secos
* Doritos
* Patatas fritas
* Tabla de jamón
* Tabla de quesos
* Nachos


=== Platos principales

* Chuletas de cabeza
* Churrasco de ternera
* Croquetas de jamón
* Hamburguesa
* Pinchitos de pollo
* Salchichas
* Tortilla de patata


=== Postres

* Arroz con leche
* Natilla
* Peras al vino tinto
* Queso fresco
* Tarta de queso
* Tarta de tres chocolates
* Yogurt de coco
